---
layout: page
title: About
permalink: /about/
---

We are a community of programmers based out of the #dailyprog IRC channel on Rizon. Our members hail from countries around the world, including the US, Canada, France, Germany, Turkey, Italy, Greece, Brazil, and more. Our members represent a cross-section of disciplines within software engineering and technology.

Programmers of all skill levels are welcome in our community. We offer each other code reviews, advice, criticism, and camaraderie. Stop by sometime and let's talk about what you are working on.

You can find our GitLab group here:
[dailyprog](https://gitlab.com/dailyprog)
